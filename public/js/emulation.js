
  
    
    
    function sendEmulator(token, url, remoteUrl, sum, commission, orderNumber){
		console.log(url);
		$.post({
            url: url,
			data: {
				'_token': 		token,
				'remote-url': 	remoteUrl,
				'sum':			sum,
				'commission': 	commission,
				'order-number': orderNumber,
			},
            dataType: 'json',
            success: function(jqXhr) {   
                let text = '<div class="response"><p>Пакет сохранен!</p></div>';
			   $('#result-container').append(text);
			},
            error: function (jqXhr) {
                let text = jqXhr.responseJSON;
			   $('#result-container').append('<div class="response"><p>Ошибка!' + Object.values(text) + '</p></div>');
			},
            fail: function(jqXhr){
                let text = jqXhr.responseJSON;
				$('#result-container').append('<div class="response"><p>Ошибка!' + Object.values(text) + '</p></div>');
			}
        });
	}
		
	function checkAndSubmit () {
	
		let data    = $('#emulForm').serializeArray();
		let token  			= data[0].value;
		let url     		= data[1].value;
		let remoteUrl    	= data[2].value;
		
		let error = false;
		
		if (remoteUrl ==='') 
		{
			error = true;
			alert('Поле URL сервиса обязательно к заполнению');
		}
		
		let RegExp = /((http|https):\/\/)?(www\.)?([A-Za-zА-Яа-я0-9]{1}[A-Za-zА-Яа-я0-9\-]*\.?)*\.{1}[A-Za-zА-Яа-я0-9-]{2,8}(\/([\w#!:.?+=&%@!\-\/])*)?/;
		
		if(!RegExp.test(remoteUrl)) {
			error = true;
			alert('Поле "URL сервиса" должно быть в формате URL');
		}
		
		if (!error)
		{
			let sum 		= [];
			let commission	= [];
			let orderNumber	= [];

			for (let i = 0; i < getRandomInt(1, 20); i++ )
			{
				sum.push(getRandomInt(10, 500));
				commission.push(getRandomFloat(0.5, 2.0));
				orderNumber.push(getRandomInt(1, 20));
			}
			console.log(sum);
			sendEmulator(token, url, remoteUrl, sum, commission, orderNumber);	

		}
	}
	
    
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
    }

	$('#emulSubmit').on('click', function () {		
        checkAndSubmit ();
	});
	
	$('#emulForm').on('submit', function () {	
		event.preventDefault();	
		checkAndSubmit ();
	});
	
	
	
