<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Эмулятор отсылки платежей</title>
    </head>
    <body>
    
    
    

        <div class="flex-center position-ref full-height">
            
            <div class="content">
                
                <form method="post" id="emulForm">
                    
                    
                    <label> Введите URL принимающего сервиса:</label> <input type="text" name="data-remote-url" id="remote-url">
                    <button type="button" id="emulSubmit">Начать эмуляцию</button>
                </form>
                <div id='result-container'>
                </div>
            </div>

        </div>




    <script
    src="https://code.jquery.com/jquery-3.5.0.min.js"
    integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
    <script src="/js/emulation.js"></script>
    </body>
</html>
