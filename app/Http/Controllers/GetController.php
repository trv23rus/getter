<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use GuzzleHttp\Client;
use App\Jobs\SaveData;


class GetController extends Controller
{
    

    public function __construct()
    {
      	//
    }

    
    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * 
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            
          ]);
        
         
            $this->dispatch(new SaveData($request->all())); 
          
    }

}
