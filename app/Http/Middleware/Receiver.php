<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;



class Receiver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Api-Secret');
        if ($token == getenv('SECRET'))
        {
            return $next($request);
        }
        return response('Not Authorized', 401);
    }
}
