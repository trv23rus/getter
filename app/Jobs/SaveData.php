<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Payment;
use App\UserWalet;

class SaveData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $dataEntity) 
        {
            $payment = new Payment();
            $payment->transmitted_id    = $dataEntity[0];
            $payment->full_sum          = $dataEntity[1]+($dataEntity[1]/100*$dataEntity[2]);
            $payment->order_number      = $dataEntity[3];
            $payment->save();

            $walet = new UserWalet();
            $update = $walet->where('user_id', $dataEntity[3])->first();
            if (!$update) 
            {
                $walet->user_id     = $dataEntity[3];
                $walet->sum         = $dataEntity[1]+($dataEntity[1]/100*$dataEntity[2]);
                $walet->save();
            }
            else
            {
                $update->sum        = $update->sum + $dataEntity[1]+($dataEntity[1]/100*$dataEntity[2]);
                $update->save();
            }
            

        }
        
        //
    }
}
